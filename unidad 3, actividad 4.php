<html>
<header><b>Registro</b></header>
<body>

<form method="post">
Clave   :<input type="Text" name="clave"><br>
Nombre	:<input type="Text" name="nombre"><br>
Apellido:<input type="Text" name="apellido"><br>
Correo	:<input type="Text" name="correo"><br>
Calle	:<input type="Text" name="calle"><br>
Colonia	:<input type="Text" name="colonia"><br>
CP		:<input type="Text" name="cp"><br>
Ciudad	:<input type="Text" name="ciudad"><br>
País	:<input type="Text" name="pais"><br>
Teléfono:<input type="Text" name="telefono"><br>
<input type="Submit" name="enviar" value="Aceptar información">
</form>
<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/vendor/doctrine/common/lib/Doctrine/Common/ClassLoader.php';

use Doctrine\Common\ClassLoader;
$app = new Silex\Application();
$classLoader = new ClassLoader('Doctrine', '/path/to/doctrine');
$classLoader->register();

$config = new \Doctrine\DBAL\Configuration();

$connectionParams = array(
    'dbname' => 'carrito',
    'user' => 'root',
    'host' => 'localhost',
    'charset' => 'utf8',
    'driver' => 'pdo_mysql',
);
$app = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

if(isset($_POST))
{
	$clave=$_POST['clave'];
	$nombre=$_POST['nombre'];
	$apellido=$_POST['apellido'];
	$correo=$_POST['correo'];
	$calle=$_POST['calle'];
	$colonia=$_POST['colonia'];
	$cp=$_POST['cp'];
	$ciudad=$_POST['ciudad'];
	$pais=$_POST['pais'];
	$telefono=$_POST['telefono'];
	$app->insert('clientes', array('clave' => $clave, 'nombre' => $nombre, 'apellido' => $apellido, 'correo' => $correo, 'calle' => $calle, 'cp' => $cp, 'ciudad' => $ciudad, 'pais' => $pais, 'telefono' => $telefono, 'fecha_creacion' => date("c"), 'colonia' => $colonia));
}
?>
</body>
</html>